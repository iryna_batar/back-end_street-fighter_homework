const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
    create(data) {
        const FIGHTERS = FighterRepository.getAll();
        for (let fighter of FIGHTERS) {
            if (fighter.name.toLowerCase() === data.name.toLowerCase()) {
                throw {error: 'Fighter with this name already exist', code: 400};
            }
        }

        let fighter = {
            name: data.name,
            health: data.health ? data.health : 100,
            power: data.power,
            defense: data.defense,
        };

        return FighterRepository.create(fighter);
    }

    update(id, data) {
        if (!FighterRepository.getOne({id: id})) {
            throw Error('Fighter with this id is not exist');
        }

        if(data.name) {
            const FIGHTERS = FighterRepository.getAll();
            for (let fighter of FIGHTERS) {
                if (fighter.name.toLowerCase() === data.name.toLowerCase() && fighter.id !== id) {
                    throw {error: 'Fighter with this name already exist', code: 400};
                }
            }
        }

        return FighterRepository.update(id, data);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            throw Error('Fighter with this id is not exist');
        }
        return item;
    }

    delete(id) {
        if (!FighterRepository.getOne({id: id})) {
            throw Error('Fighter with this id is not exist');
        }

        return FighterRepository.delete(id);
    }

    getAll() {

        return FighterRepository.getAll();
    }

    getOne(id) {
        const item = FighterRepository.getOne({id: id});
        if (!item) {
            throw Error('Fighter with this id is not exist');
        }
        return item;
    }
}

module.exports = new FighterService();