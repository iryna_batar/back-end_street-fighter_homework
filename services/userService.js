const {UserRepository} = require('../repositories/userRepository');

class UserService {

    create(data) {

        if (UserRepository.getOne({email: data.email})) {
            throw {error: 'User with this email already exist', code: 400};
        }
        if (UserRepository.getOne({phoneNumber: data.phoneNumber})) {
            throw {error: 'User with this phone number already exist', code: 400};
        }
        let user = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            phoneNumber: data.phoneNumber,
            password: data.password
        };

        return UserRepository.create(user);
    }

    update(id, data) {

        if (UserRepository.getOne(id)) {
            throw Error('User is not exist');
        }

        if (data.email) {
            const CHECK_EMAIL = UserRepository.getOne({email: data.email});
            if (CHECK_EMAIL) {
                if (CHECK_EMAIL.id !== id) {
                    throw {error: 'User with this email already exist', code: 400};
                }
            }
        }

        if (data.phoneNumber) {
            const CHECK_PHONE = UserRepository.getOne({phoneNumber: data.phoneNumber});

            if (CHECK_PHONE) {
                if (CHECK_PHONE.id !== id) {
                    throw {error: 'User with this phone number already exist', code: 400};
                }
            }
        }

        return UserRepository.update(id, data);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            throw Error('User with this id is not exist');
        }

        return item;
    }

    delete(id) {

        if (!UserRepository.getOne({id: id})) {
            throw Error('User with this id is not exist');
        }

        return UserRepository.delete(id);
    }

    getAll() {

        return UserRepository.getAll();
    }

    getOne(id) {

        const item = UserRepository.getOne({id: id});
        if (!item) {
            throw Error('User with this id is not exist');
        }

        return item;
    }
}

module.exports = new UserService();