const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    console.log(req.body)
    try {
        let data = AuthService.login(req.body);
        console.log(data)
        if (data) {
            console.log(data);
            res.data = data;

        } else {
            res.status(400).send('User is not exist');
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;