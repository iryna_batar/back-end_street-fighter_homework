const responseMiddleware = (req, res, next) => {

    if (res.err) {
        if(res.err.code) {
            res.status(res.err.code).send({
                message: res.err.error,
                error: true,
            })
        } else {
            res.status(404).send({
                message: res.err.toString(),
                error: true,
            })
        }

    } else {
        res.send(res.data)
    }
    next();
}

exports.responseMiddleware = responseMiddleware;