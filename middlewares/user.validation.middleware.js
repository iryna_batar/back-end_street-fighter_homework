const {user} = require('../models/user');
const createUserValid = (req, res, next) => {

    if (!req.body || Object.keys(req.body).length !== Object.keys(user).length - 1) {
        res.status(400).send({
            message: 'Fill all field',
            error: true,
        });
        return;
    }
    for (let key of Object.keys(user).filter(key => key !== 'id')) {
        if (!Object.keys(req.body).includes(key)) {
            res.status(400).send({
                message: key + " required",
                error: true,
            });
            return;
        }
    }
    const EMAIL_CHECK = /(@gmail\.com)$/;
    const PHONE_CHECK = /^(\+380)/;

    if (!EMAIL_CHECK.test(req.body.email)) {
        res.status(400).send({
            message: 'Invalid type of email',
            error: true,
        });
        return;
    }

    if (!PHONE_CHECK.test(req.body.phoneNumber)) {
        res.status(400).send({
            message: 'Invalid type of phone number',
            error: true,
        });
        return;
    }

    if (req.body.password.length < 3) {
        res.status(400).send({
            message: 'Password need to be more than 3 char',
            error: true,
        });
        return;
    }

    next();
}

const updateUserValid = (req, res, next) => {

    for (let key of Object.keys(req.body)) {
        if (!Object.keys(user).filter(key => key !== 'id').includes(key)) {
            res.status(400).send({
                message: key + " is not existed.",
                error: true,
            });
            return;
        }
    }

    if(req.body.email) {
        const EMAIL_CHECK = /(@gmail\.com)$/;
        if (!EMAIL_CHECK.test(req.body.email)) {
            res.status(400).send({
                message: 'Invalid type of email',
                error: true,
            });
            return;
        }
    }

    if(req.body.phoneNumber) {
        const PHONE_CHECK = /^(\+380)/;
        if (!PHONE_CHECK.test(req.body.phoneNumber)) {
            res.status(400).send({
                message: 'Invalid type of phone number',
                error: true,
            });
            return;
        }
    }

    if(req.body.password) {
        if (req.body.password.length < 3) {
            res.status(400).send({
                message: 'Password need to be more than 3 char',
                error: true,
            });
            return;
        }
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;