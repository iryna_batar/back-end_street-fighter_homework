const {fighter} = require('../models/fighter');

const createFighterValid = (req, res, next) => {

    if(!req.body || req.body.id) {
        res.status(400).send({
            message: 'Fill only required field',
            error: true,
        })
        return;
    }

    if (Object.keys(req.body).length !== Object.keys(fighter).length - (req.body.health ? 1 : 2)) {
        res.status(400).send({
            message: 'Fill all field',
            error: true,
        });
        return;
    }

    for (let key of Object.keys(fighter).filter(key => key !== 'id' && key !== 'health')) {
        if (!Object.keys(req.body).includes(key)) {
            res.status(400).send({
                message: key + " required",
                error: true,
            });
            return;
        }
    }

    if (req.body.health) {
        if (!isNumeric(req.body.health) || req.body.health < 80  || req.body.health > 120) {
            res.status(400).send({
                message: 'Health must to be in this diapason: 80 - 120',
                error: true,
            });
            return;
        }
    }

    if (!isNumeric(req.body.power) || 1 > req.body.power || req.body.power > 100) {
        res.status(400).send({
            message: 'Power must to be in this diapason: 1 - 100',
            error: true,
        });
        return;
    }

    if (!isNumeric(req.body.defense) || 1 > req.body.defense || req.body.defense > 10) {
        res.status(400).send({
            message: 'Defense must to be in this diapason: 1 - 10',
            error: true,
        });
        return;
    }

    next();
}

function isNumeric(str) {
    if (typeof str != "string") return false
    return !isNaN(str) &&
        !isNaN(parseFloat(str))
}

const updateFighterValid = (req, res, next) => {

    for (let key of Object.keys(req.body)) {
        if (!Object.keys(fighter).filter(key => key !== 'id').includes(key)) {
            res.status(400).send({
                message: key + " is not exist",
                error: true,
            });
            return;
        }
    }

    if(!req.body || req.body.id) {
        res.status(400).send({
            message: 'Fill only required field',
            error: true,
        });
        return;
    }

    if (req.body.health) {
        if (!isNumeric(req.body.health) || req.body.health < 80  || req.body.health > 120) {
            res.status(400).send({
                message: 'Health must to be in this diapason: 80 - 120',
                error: true,
            });
            return;
        }
    }

    if(req.body.power) {
        if (!isNumeric(req.body.power) || 1 > req.body.power || req.body.power > 100) {
            res.status(400).send({
                message: 'Power must to be in this diapason: 1 - 100',
                error: true,
            });
            return;
        }
    }

    if(req.body.defense) {
        if (!isNumeric(req.body.defense) || 1 > req.body.defense || req.body.defense > 10) {
            res.status(400).send({
                message: 'Defense must to be in this diapason: 1 - 10',
                error: true,
            });
            return;
        }
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;